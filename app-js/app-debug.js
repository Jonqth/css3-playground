$(document).ready(function(){
	$('#cssdebug').toggle(function(){
		$('header, nav,#jonathanlevy-avat_container,#jonathanlevy_inner-container, footer').each(function(i,item){
			$(item).css('border','1px solid #'+Math.floor(Math.random()*16777215).toString(16));
		});
	},function(){
		$('header, nav,#jonathanlevy-avat_container,#jonathanlevy_inner-container, footer').css('border','none');
	});
});