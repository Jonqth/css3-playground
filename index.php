<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!-- CSS -->
<link href="app-css/main.css" type="text/css" rel="stylesheet" />

<!-- JS -->
<script src="app-js/app-jquery.js" type="application/javascript"></script>
<script src="app-js/app-main.js" type="application/javascript"></script>

<title>Jonathan Levy</title>
</head>
<body>

    <!-- menu -->
    <nav>
        <ul>
            <li>
            	<a href="#works" title="Selected Works">
                	<img src="app-assets/img/jonathanlevy_works.png" alt="Selected Works" />
                    <p>works</p>
                </a>
            </li>
            <li>
            	<a href="#likes" title="What i like !">
                	<img src="app-assets/img/jonathanlevy_likes.png" alt="What i like !" />
                    <p>likes</p>
                </a>
            </li>
            <li>
            	<a href="#about" title="Know more about me">
                	<img src="app-assets/img/jonathanlevy_about.png" alt="Know more about me" />
                    <p>about</p>
                </a>
            </li>
            <li>
            	<a href="#contact" title="Contact me">
                	<img src="app-assets/img/jonathanlevy_contact.png" alt="Contact me" />
                    <p>contact</p>
                </a>
            </li>
            <li>
            	<a href="#stuff" title="Some Stuff">
                	<img src="app-assets/img/jonathanlevy_stuff.png" alt="Some Stuff" />
                    <p>stuff</p>
                </a>
            </li>
        </ul>
    </nav>
    <!-- /menu -->
    
    <!-- main-container begin -->
    <div id="jonathanlevy_main-container"> 
    
        <!-- header begin -->
        <header id="jonathanlevy_header">
            <h1>jalehouse</h1>
            
            <div id="jonathanlevy-avat_container">
            
                <h2>
                    jonathan
                    <aside>levy</aside>
                </h2>
                <img src="app-img/jonathanlevy_avatar.png" alt="Jonathan Levy, D&eacute;veloppeur." />
                <div class="jonathanlevy_clearer"></div>
                
            </div>
            
            <div class="jonathanlevy_clearer"></div>
        </header>
        <!-- header ends -->
        
        <!-- inner-container begin -->
        <div id="jonathanlevy_inner-container">
        
        	<!-- pages-container begin -->
        	<div id="jonathanlevy_pages-container">
            
				<?php for($i=0; $i<2; $i++) { ?>
                    <div class="jonathanlevy_pages">
                        <!-- display -->
                        <section class="middle">
                            
                            	                        <!-- tip --> 
                                <a href="#" title="laval virtual">                     
                                <header>
                                	
                                    <div class="jonathanlevy_title-container">
                                        <h1>laval virtual</h1>
                                        <h2><span>graphic</span>, <span>concours</span></h2>
                                    </div> 
          							
                                    <div class="jonathanlevy_next-container">
                                    	<img src="app-assets/img/jonathanlevy_display.png" alt="display it" />
                                    </div>
                                    
                                </header>
                                </a>
                           
                          
                            <!-- content -->
                            <div class="jonathanlevy_display-container">
                            	<img src="app-img/jonathanlevy_test.png" alt="content" />
                                <div class="jonathanlevy_display-panel">
                                	<p>show me more</p>
                                </div>
                            </div>
                            
                        </section>
                        
                        <!-- display -->
                        <section class="middle">                            	
                        
                                <!-- tip --> 
                                <a href="#" title="laval virtual">                     
                                <header>
                                	
                                    <div class="jonathanlevy_title-container">
                                        <h1>laval virtual</h1>
                                        <h2><span>graphic</span>, <span>concours</span></h2>
                                    </div> 
          
                                    <div class="jonathanlevy_next-container">
                                    	<img src="app-assets/img/jonathanlevy_display.png" alt="display it" />
                                    </div>
                                    
                                </header>
                                </a>
                            
                            <!-- content -->
                            <div class="jonathanlevy_display-container">
                            	<img src="app-img/jonathanlevy_test.png" alt="content" />
                                <div class="jonathanlevy_display-panel">
                                	<p>show me more</p>
                                </div>
                            </div>
                            
                        </section>
                        
                        <!-- display -->
                        <section class="tiny">                            	                       
                        
                        	 <!-- tip --> 
                                <a href="#" title="laval virtual">                     
                                <header>
                                	
                                    <div class="jonathanlevy_title-container">
                                        <h1>laval virtual</h1>
                                        <h2><span>graphic</span>, <span>concours</span></h2>
                                    </div> 
          
                                    <div class="jonathanlevy_next-container">
                                    	<img src="app-assets/img/jonathanlevy_display.png" alt="display it" />
                                    </div>
                                    
                                </header>
                                </a>
                            
                            <!-- content -->
                            <div class="jonathanlevy_display-container">
                            	<img src="app-img/jonathanlevy_test.png" alt="content" />
                                <div class="jonathanlevy_display-panel">
                                	<p>show me more</p>
                                </div>
                            </div>
                            
                        </section>
                        
                        <!-- display -->
                        <section class="tiny">                            	                        
                        
                        	<!-- tip --> 
                                <a href="#" title="laval virtual">                     
                                <header>
                                	
                                    <div class="jonathanlevy_title-container">
                                        <h1>laval virtual</h1>
                                        <h2><span>graphic</span>, <span>concours</span></h2>
                                    </div> 
          
                                    <div class="jonathanlevy_next-container">
                                    	<img src="app-assets/img/jonathanlevy_display.png" alt="display it" />
                                    </div>
                                    
                                </header>
                                </a>
                            
                            <!-- content -->
                            <div class="jonathanlevy_display-container">
                            	<img src="app-img/jonathanlevy_test.png" alt="content" />
                                <div class="jonathanlevy_display-panel">
                                	<p>show me more</p>
                                </div>
                            </div>
                            
                        </section>
                        
                        <!-- display -->
                        <section class="big">                            	                        
                        
                        	<!-- tip --> 
                                <a href="#" title="laval virtual">                     
                                <header>
                                	
                                    <div class="jonathanlevy_title-container">
                                        <h1>laval virtual</h1>
                                        <h2><span>graphic</span>, <span>concours</span></h2>
                                    </div> 
          
                                    <div class="jonathanlevy_next-container">
                                    	<img src="app-assets/img/jonathanlevy_display.png" alt="display it" />
                                    </div>
                                    
                                </header>
                                </a>
                            
                            <!-- content -->
                            <div class="jonathanlevy_display-container">
                            	<img src="app-img/jonathanlevy_test.png" alt="content" />
                                <div class="jonathanlevy_display-panel">
                                	<p>show me more</p>
                                </div>
                            </div>
                            
                        </section>
                        
                        <!-- display -->
                        <section class="middle">                            	                        
                        
                        	<!-- tip --> 
                                <a href="#" title="laval virtual">                     
                                <header>
                                	
                                    <div class="jonathanlevy_title-container">
                                        <h1>laval virtual</h1>
                                        <h2><span>graphic</span>, <span>concours</span></h2>
                                    </div> 
          
                                    <div class="jonathanlevy_next-container">
                                    	<img src="app-assets/img/jonathanlevy_display.png" alt="display it" />
                                    </div>
                                    
                                </header>
                                </a>
                            
                            <!-- content -->
                            <div class="jonathanlevy_display-container">
                            	<img src="app-img/jonathanlevy_test.png" alt="content" />
                                <div class="jonathanlevy_display-panel">
                                	<p>show me more</p>
                                </div>
                            </div>
                            
                        </section>
                        
                        <!-- display -->
                        <section class="middle">                            	                        
                        
                        	<!-- tip --> 
                                <a href="#" title="laval virtual">                     
                                <header>
                                	
                                    <div class="jonathanlevy_title-container">
                                        <h1>laval virtual</h1>
                                        <h2><span>graphic</span>, <span>concours</span></h2>
                                    </div> 
          
                                    <div class="jonathanlevy_next-container">
                                    	<img src="app-assets/img/jonathanlevy_display.png" alt="display it" />
                                    </div>
                                    
                                </header>
                                </a>
                            
                            <!-- content -->
                            <div class="jonathanlevy_display-container">
                            	<img src="app-img/jonathanlevy_test.png" alt="content" />
                                <div class="jonathanlevy_display-panel">
                                	<p>show me more</p>
                                </div>
                            </div>
                            
                        </section>
                        
                        <!-- display -->
                        <section class="middle">                            	                        
                        
                        	<!-- tip --> 
                                <a href="#" title="laval virtual">                     
                                <header>
                                	
                                    <div class="jonathanlevy_title-container">
                                        <h1>laval virtual</h1>
                                        <h2><span>graphic</span>, <span>concours</span></h2>
                                    </div> 
          
                                    <div class="jonathanlevy_next-container">
                                    	<img src="app-assets/img/jonathanlevy_display.png" alt="display it" />
                                    </div>
                                    
                                </header>
                                </a>
                            
                            <!-- content -->
                            <div class="jonathanlevy_display-container">
                            	<img src="app-img/jonathanlevy_test.png" alt="content" />
                                <div class="jonathanlevy_display-panel">
                                	<p>show me more</p>
                                </div>
                            </div>
                            
                        </section>
                        
                        <!-- display -->
                        <section class="middle">
                            
                            <!-- tip --> 
                                <a href="#" title="laval virtual">                     
                                <header>
                                	
                                    <div class="jonathanlevy_title-container">
                                        <h1>laval virtual</h1>
                                        <h2><span>graphic</span>, <span>concours</span></h2>
                                    </div> 
          
                                    <div class="jonathanlevy_next-container">
                                    	<img src="app-assets/img/jonathanlevy_display.png" alt="display it" />
                                    </div>
                                    
                                </header>
                                </a>
                            
                            <!-- content -->
                            <div class="jonathanlevy_display-container">
                            	<img src="app-img/jonathanlevy_test.png" alt="content" />
                                <div class="jonathanlevy_display-panel">
                                	<p>show me more</p>
                                </div>
                            </div>
                            
                        </section>
        
                    </div>
                <?php } ?>
                
            </div>
        	<!-- pages-container ends -->

        </div>
        <!-- inner-container ends -->
        
     </div>
     <!-- main-container ends -->
     
     <!-- footer begin -->
     <footer>
     </footer>
     <!-- footer ends -->

</body>
</html>